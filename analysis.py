import scipy, os, functools
import numpy as np
import subprocess
from ase import Atoms
from ase import units as u
from ase.io import Trajectory, write, read
from ase.calculators.singlepoint import SinglePointCalculator as SPC
from ase.io.trajectory import TrajectoryWriter as TW
from pathlib import Path
from tqdm.auto import tqdm
import nglview as nv
import matplotlib.pyplot as plt
import rmsd

# used to translate to ORCA input
cpcm = {'vacuum':'',
        'THF':'CPCM(THF)',
        'toluene': 'CPCM(toluene)',
        'acetonitrile':'CPCM(acetonitrile)'}

def get_angle(v1, v2):
    u1 = v1 / np.linalg.norm(v1)
    u2 = v2 / np.linalg.norm(v2)
    ang = np.arctan2(np.linalg.norm(np.cross(u1, u2)), np.dot(u1, u2))
    return np.degrees(ang)

def weigh(energies, mask=None, T=300):
    if mask is None:
        mask = np.ones(len(energies), bool)
    R =  (8.3145e-3 * u.kJ / u.mol) / (u.kcal / u.mol)
    # Global or masked min?
    delta_e = (energies[mask] - np.min(energies)) * u.Hartree / (u.kcal / u.mol)
    p = np.exp(-delta_e / (R * T)) / np.sum(np.exp(-delta_e / (R * T)))
    return p[:, None]


def align(idx, atoms, ref):
    ''' Align the [idx] atoms to the positions of ref[idx]
        assuming the idx is the same for atoms and ref.

        aligning in place
    '''

    ref_pos = ref[idx].get_positions()
    atm_pos = atoms[idx].get_positions()

    # Translate atoms to ref
    ref_c = rmsd.centroid(ref_pos)
    atm_c = rmsd.centroid(atm_pos)
    atm_pos += (ref_c - atm_c)

    # Get rotation matrix via the Kabsch algo
    U = rmsd.kabsch(ref_pos, atm_pos)

    pos = atoms.get_positions() + (ref_c - atm_c)
    new_pos = np.dot(pos, U.T)


    return new_pos

t1_idx = [31, 30, 35, 36]
t2_idx = [32, 27, 10,  7]

def calc_shizu(traj):
    angles = np.zeros((len(traj), 2))
    for i, atoms in enumerate(traj):
        angles[i, 0] = atoms.get_dihedral(*t1_idx)
        angles[i, 1] = atoms.get_dihedral(*t2_idx)

    return angles

class PlaneFitter:
    def __init__(self, points):
        self.points = points

    def plane(self, x, y, params):
        a = params[0]
        b = params[1]
        c = params[2]
        z = a*x + b*y + c
        return z

    def error(self, params, points):
        result = 0
        for (x,y,z) in points:
            plane_z = self.plane(x, y, params)
            diff = abs(plane_z - z)
            result += diff**2
        return result

    def cross(self, a, b):
        return [a[1] * b[2] - a[2] * b[1],
                a[2] * b[0] - a[0] * b[2],
                a[0] * b[1] - a[1] * b[0]]

    def fit(self):
        fun = functools.partial(self.error, points=self.points)
        params0 = [0, 0, 0]
        res = scipy.optimize.minimize(fun, params0)
        a = res.x[0]
        b = res.x[1]
        c = res.x[2]

        point  = np.array([0.0, 0.0, c])
        normal = np.array(self.cross([1, 0, a], [0, 1, b]))
        d = - point.dot(normal)

        self.res = res
        return normal, d


class Korovina:
    def __init__(self, mol='ehex',
                 conf_path='../2023_May/korovina_systems/CREST/ehex/s0_toluene/crest_conformers.xyz'):
        self.mol = mol
        self.path = conf_path

        # XXX These aren't used anymore
        if mol == 'ehex':  # vectors pointing _the same way_ across the two chromophohes
            self.v1idx = [46, 51]
            self.v2idx = [20, 4]
        elif mol == 'ohex':
            self.v1idx = [46, 51]
            self.v2idx = [20, 4]
        elif mol == 'cmdl':
            self.v1idx = [46, 51]
            self.v2idx = [20, 4]
        else:
            raise NotImplementedError(f'{mol} not implemented')

        self.energies = None
        self.traj = None
        self.angles = None

    def read_conformers(self):
        traj = read(self.path, index=':', format='xyz')
        self.traj = traj

    def read_energies(self):
        ''' Reading absolute energies from the conformer xyz.
            This function is NOT very robust, as it just looks for the first line of
            each frame, which should be the same, and then takes the next line which
            contains the energy.
        '''
        with open(self.path, 'r') as f:
            lines = f.readlines()
        energies = np.zeros(len(self.traj))
        ct = 0
        for i, line in enumerate(lines):
            if line == lines[0]:
                energies[ct] = float(lines[i + 1].split()[0])
                ct +=1

        self.energies = energies

    def calc_angle_old(self, atoms):
        ''' DEPRECATED '''
        r = Rotator(atoms, mol=self.mol)
        c1 = r.idx_ch1
        c2 = r.idx_ch2

        normals = []
        for idx in [c1, c2]:
            pf = PlaneFitter(atoms[idx].get_positions())
            normal, _ = pf.fit()
            normals.append(normal)
        angle = get_angle(*normals)

        idx1 = self.v1idx
        idx2 = self.v2idx

        v1 = atoms[idx1[0]].position - atoms[idx1[1]].position
        v2 = atoms[idx2[0]].position - atoms[idx2[1]].position

        if np.dot(v1 / np.linalg.norm(v1), v2 / np.linalg.norm(v2)) < 0:
            angle = 180 - angle

        return angle

    def calc_angle_180(self, atoms):
        ''' 0-180 only, i.e. no direction '''
        # simpler normals - the indices are the same for both ohex and ehex and the model system! confirmed!
        pos = atoms.get_positions()
        v1_1 = pos[52] - pos[45]
        v1_1 /= np.linalg.norm(v1_1)
        v1_2 = pos[35] - pos[38]
        v1_2 /= np.linalg.norm(v1_2)
        n1 = np.cross(v1_1, v1_2)

        v2_1 = pos[21] - pos[9]
        v2_1 /= np.linalg.norm(v2_1)
        v2_2 = pos[10] - pos[13]
        v2_2 /= np.linalg.norm(v2_2)
        n2 = np.cross(v2_1, v2_2)

        normals = [n1, n2]
        angle = get_angle(*normals)

        return angle


    def calc_angle(self, atoms):
        ''' Including direction '''
        idx = {'ohex': (29, 32), 'ehex': (59, 73), 'cmdl':(32, 29)}
        # simpler normals - the indices are the same for both ohex and ehex and the model system! confirmed!
        pos = atoms.get_positions()
        v1_1 = pos[45] - pos[52]
        v1_1 /= np.linalg.norm(v1_1)
        v1_2 = pos[35] - pos[38]
        v1_2 /= np.linalg.norm(v1_2)
        n1 = np.cross(v1_1, v1_2)

        v2_1 = pos[21] - pos[9]
        v2_1 /= np.linalg.norm(v2_1)
        v2_2 = pos[10] - pos[13]
        v2_2 /= np.linalg.norm(v2_2)
        n2 = np.cross(v2_1, v2_2)

        normals = [n1, n2]
        angle = 180 - get_angle(*normals)

        # Direction
        i = idx[self.mol]
        ref = pos[i[0]] - pos[i[1]]
        ref /= np.linalg.norm(ref)
        direction = np.dot(np.cross(n1, n2), ref)
        if direction < 0:
            angle = 360 - angle

        return angle

    def get_angles(self):
        if self.traj is None:
            self.read_conformers()
        if self.energies is None:
            self.read_energies()

        angles = np.zeros(len(self.energies))
        for i, atoms in tqdm(enumerate(self.traj), total=len(self.traj)):
            angles[i] = self.calc_angle(atoms)

        self.angles = angles

    def get_angles_old(self):
        '''
            DEPRECATED! Gives angle discontinuities
            Uses the atom idices in Rotator to define planes of chromophores
            But we also need vectors across each chromophore for
            the definition of rotation direction.
        '''
        if self.traj is None:
            self.read_conformers()
        if self.energies is None:
            self.read_energies()

        r = Rotator(self.traj[0], mol=self.mol)

        c1 = r.idx_ch1
        c2 = r.idx_ch2

        angles = np.zeros(len(self.energies))
        for i, atoms in tqdm(enumerate(self.traj), total=len(self.traj)):
            normals = []
            for idx in [c1, c2]:
                pf = PlaneFitter(atoms[idx].get_positions())
                normal, _ = pf.fit()
                normals.append(normal)
            angle = get_angle(*normals)

            idx1 = self.v1idx
            idx2 = self.v2idx

            v1 = atoms[idx1[0]].position - atoms[idx1[1]].position
            v2 = atoms[idx2[0]].position - atoms[idx2[1]].position

            if np.dot(v1 / np.linalg.norm(v1), v2 / np.linalg.norm(v2)) < 0:
                angle = -angle

            angles[i] = angle

        self.angles = angles

    def plot_hist(self, bins=np.arange(0, 370, 10), ax=None, show_planar=False, **plot_args):
        if self.angles is None:
            self.get_angles()

        hist, bins = np.histogram(self.angles,
                               bins=bins,
                               density=False,
                               weights=weigh(np.array(self.energies)).squeeze())
        fig = None
        if ax is None:
            fig, ax = plt.subplots(1, 1, figsize=(8, 5))
        if show_planar:
            ax.axvline(180, label='Antiparallel', color='k')
        ax.plot(bins[1:] - 0.5 * np.diff(bins)[0], hist, **plot_args)
        ax.ticklabel_format(style='plain')
        ax.set_xlim([bins[0], bins[-1]])
        ax.set_ylabel('Weighed Pop.')
        ax.set_xlabel('Chromophore rotation (negative = flipped) [deg.]')
        ax.legend(loc='upper left');

        return fig, ax

    def find_conformer_extremes(self):
        if self.angles is None:
            self.get_angles()
        if self.energies is None:
            self.read_energies()
        if self.traj is None:
            self.read_conformers()

        angles = self.angles
        idx_max = np.argmax(angles)  # minimum angle
        idx_min = np.argmin(angles)  # maximum
        idx_zro = np.argmin(np.abs(angles))  # closest to planar
        idx_ene = np.argmin(self.energies)  # minimum energy (always index 0, but...)

        extremes = [self.traj[i] for i in [idx_max, idx_min, idx_zro, idx_ene] ]
        self.extremes = extremes
        self.extreme_energies = [self.energies[i] for i in [idx_max, idx_min, idx_zro, idx_ene] ]


    def generate_orca(self, outdir, env, state):
        funcs = ['B3LYPD3', 'B3LYPD4', 'LCBLYP', 'wB97X-D3BJ']
        states = ['s0']

        fdict = {'wB97X-D3BJ':'wB97X-D3BJ',
                'B3LYPD3':'B3LYP D3',  # because the HPC Orca might have the buggy D4..
                'B3LYPD4':'B3LYP D4',
                'LCBLYP': 'LC-BLYP'}

        mult = {'s0':'1', 'q1':'5', 's1':1}

        tddft = '%tddft IRoot 1 end'

        exts = ['amin', 'amax', 'apln', 'emin']
        for atoms, ext in zip(self.extremes, exts):
            pos = atoms.get_positions()
            atm = atoms.get_chemical_symbols()

            #xyz = '\n'.join([''.join([f'{val:15.10f}' for val in row]) for row in pos])

            for func in funcs:
                this_dir = outdir + f'/{func}/{ext}'
                os.makedirs(f'{this_dir}', exist_ok=True)

                head = f'! UKS {fdict[func]} def2-SVP def2/J TIGHTSCF Opt {cpcm[env]}'
                for extra_state in [state, 's1', 'q1']:
                    inp = this_dir + f'/{extra_state}_{func}_{ext}_opt.inp'
                    with open(inp, 'w') as f:
                        f.write(head)
                        f.write('\n')
                        f.write('\n')
                        f.write('%pal\nnprocs 16\nend')
                        f.write('\n')
                        f.write('\n')

                        if extra_state == 's1':
                            f.write(tddft + '\n')

                        if func == 'LCBLYP':
                            f.write('%method RangeSepMu 0.29 end')
                            f.write('\n')
                        f.write(f'*xyz 0 {mult[extra_state]}\n')
                        for i, row in enumerate(pos):
                            f.write(f'{atm[i]:3s}'+''.join(f'{val:15.10f}' for val in row) + '\n')
                        f.write('*')



