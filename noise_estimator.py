'''Add noise to a Debye-signal estimated as quantum statistical noise
   following https://doi.org/10.1107/S1600577520002738. Reference data
   for standard deviation of scattering  intensity
   from https://github.com/Jkim9486/Scube.

   Thanks to  E. Selenius & G. Levi for the python-port of the Matlab code.
'''
import numpy as np
from numpy.random import normal
from grsq import Debye
from pathlib import Path
from tqdm.auto import tqdm

# reference data from 10.1107/S1600577520002738
fref = 30.0         # ref. repetition rate (Hz, pulses/sec)
nref = 1.0e+12      # ref. photons/pulse
Dref = 1 / fref              # detector exposure time
Nref = nref * fref * Dref    # photons/sec
scale_ref = 7.3094e+04       # scaling factor from exp. data

class NoiseEstimator():
    def __init__(self, 
                 accu_time, photons_pulse, reprate, exposure_time='XFEL',
                 scale=None, sigma=None, qvec=None):
        '''
        accu_time: int (seconds)
            accumulation time per scan point
        
        photons_pulse: int
            photons per Xray pulse
        
        reprate: int (pulses/sec)
            Number of xray pulses per second at the facility

        exposure time: int (sec) or 'XFEL'
            How long each detector image is exposed for.
            At XFELs you only expose for the duration of a single 
            pulse. 
        scale: float
            Leave as None to get:
            Scaling factor from experimental data. Read the SI of
            10.1107/S1600577520002738 for more info, esp. Figs S3-4
        sigma: (N, ) numpy array with N = # of q bins, or None:
            Experimentally obtained standard deviation from a solvent
            scattering signal.
            WIP: Currently auto-loads the std of cyclohexane from PAL-XFEL
        qvec: (N, ) numpy array.
            The Q-bins. If None is entered, the PAL-XFEL values will be
            loaded.


        '''

        # Get number of curves from beamline/facility parameters:
        if exposure_time == 'XFEL':  # 1 exposure per pulse
            exposure_time = 1 / reprate
        photons_sec = photons_pulse * reprate * exposure_time
        num_curves = round(accu_time / exposure_time) 
        norm = np.sqrt(photons_sec / Nref)  # normalization to reference

        here = Path(__file__).parent
        if qvec is None:
            # WIP: you could add more qvecs in here...
            qvec = np.genfromtxt(here / 'ref/q_exp_palxfel.txt')
        if sigma is None:
            sigma = np.genfromtxt(here / 'ref/std_exp_palxfel.txt')
        self.qvec = qvec
        self.deb = Debye(qvec)
        self.num_curves = num_curves
        self.sigma = sigma
        self.norm = norm
        if scale is None:
            scale = scale_ref
        self.scale = scale

    def calc_noise(self):
        q = self.qvec
        noise_on = np.zeros(q.shape)
        noise_off = np.zeros(q.shape)
        # Add numbers from normal distribution as noise:
        # length of q, num_curves_on + num_curves_off: 
        if self.num_curves < 200:  # faster, but requires huuuge memory fast.
            all_noise = normal(size=(len(q), 2 * self.num_curves))
            noise = np.sum(all_noise, axis=1)  # add along q
        else:
            noise_on = np.zeros(q.shape)
            noise_off = np.zeros(q.shape)
            # Add numbers from normal distribution as noise
            for i in tqdm(range(self.num_curves)):
                for noise in (noise_on, noise_off):
                    noise += normal(size=q.shape)


        # Since the standard deviation of the mean of I observations is
        # \sigma_m = \sigma / I^0.5 we could alternatively scale by 1/I^0.5
        # (i.e. sample from a distribution with standard deviation = \sigma_m)?
        # noise_on += normal(size = q.shape)
        # noise_off += normal(size = q.shape)
        # noise_diff = (noise_on - noise_off) * sigma / norm / I**0.5 * scale

        # Take difference, scale with std from exp. data,
        # norm. factor, number of curves, and scaling factor
        noise_diff = noise * self.sigma /\
                             self.norm /\
                             self.num_curves *\
                             self.scale

        return noise_diff

    def calculate_scaled_ds(self, atoms_off, atoms_on,
                            frac_exc, frac_yield, concentration_scale):
        ''' Calculate dS(Q) from atoms_on - atoms_off, scaled with:
            frac_exc:       The excitation fraction
            frac_yield:     The fraction of excited molecules ending in
                            The state giving the structural response
            concentration_scale:
                            Concentration of the solute
        '''
        on = self.deb.debye(atoms_on)
        off = self.deb.debye(atoms_off)

        return (on - off) * frac_exc * frac_yield / concentration_scale
